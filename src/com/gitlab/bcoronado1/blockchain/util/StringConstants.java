package com.gitlab.bcoronado1.blockchain.util;

public class StringConstants {

    public static final String SHA_256 = "SHA-256";
    public static final String UTF_8 = "UTF-8";
}
