package com.gitlab.bcoronado1.blockchain.example;

import com.gitlab.bcoronado1.blockchain.Blockchain;

public class Example {

    public static void main(String[] args){
        Blockchain blockchain = new Blockchain();

        for(Character c = '0'; c <= 'z'; c++){
            blockchain.addBlock(c.toString());
        }

        System.out.println(blockchain);
    }
}
