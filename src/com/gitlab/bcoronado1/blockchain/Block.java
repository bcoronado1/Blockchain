package com.gitlab.bcoronado1.blockchain;

import com.gitlab.bcoronado1.blockchain.util.StringUtil;

import java.util.Date;

public class Block {

    private String hash, previousHash, data;
    private Long timeStamp;

    public Block(String data, String previousHash) {
        this.data = data;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.hash = calculateHash();
    }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public String calculateHash() {
        return StringUtil.applySHA256(previousHash + Long.toString(timeStamp) + data);
    }

    @Override
    public String toString() {
        return String.format("data: %s. hash: %s. previousHash: %s", data, hash, previousHash);
    }
}