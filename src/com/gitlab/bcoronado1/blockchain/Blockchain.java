package com.gitlab.bcoronado1.blockchain;

import com.google.gson.GsonBuilder;

import java.util.concurrent.ConcurrentLinkedDeque;

public class Blockchain extends ConcurrentLinkedDeque<Block> {

    public Blockchain() {
        offer(new Block("Genesis block.", "0"));
    }

    public void addBlock(String data){
        if(isValid()){
            offer(new Block(data, getLast().getHash()));
        }
    }

    public Boolean isValid() {

        Block previous = null;

        for (Block block : this) {
            if (!block.getHash().equals(block.calculateHash())) {
                return false;
            }

            if (previous != null && !block.getPreviousHash().equals(previous.calculateHash())) {
                return false;
            }
            previous = block;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}